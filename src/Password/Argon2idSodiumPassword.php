<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Password;

/**
 * Class Argon2idSodiumPassword.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class Argon2idSodiumPassword extends SodiumCryptoPassword
{
    /**
     * @param string $plainPassword
     * @param int    $opsLimit
     * @param int    $memLimit
     *
     * @return Password
     */
    public static function fromPlainPassword(
        string $plainPassword,
        int $opsLimit = SODIUM_CRYPTO_PWHASH_OPSLIMIT_MODERATE,
        int $memLimit = SODIUM_CRYPTO_PWHASH_MEMLIMIT_MODERATE
    ): Password {
        return new static(
            sodium_crypto_pwhash_str(
                $plainPassword,
                $opsLimit,
                $memLimit
            )
        );
    }
}
