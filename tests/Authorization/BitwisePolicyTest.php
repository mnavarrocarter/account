<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Authorization;

use MNC\Account\Authorization\BitwisePolicy;
use PHPUnit\Framework\TestCase;

class BitwisePolicyTest extends TestCase
{
    public function testCreation(): void
    {
        $policy = BitwisePolicy::fromValue(
            Scope::CREATE_USERS
            + Scope::DELETE_USERS
            + Scope::IMPERSONATE_USERS
        );

        $this->assertSame(7, $policy->getValue());
    }

    public function testFailureOnNonInt(): void
    {
        $policy = BitwisePolicy::fromValue(
            Scope::CREATE_USERS
            + Scope::DELETE_USERS
            + Scope::IMPERSONATE_USERS
        );

        $this->expectException(\TypeError::class);
        $policy->grant('wrong-type-scope');
    }

    public function testCan(): void
    {
        $policy = BitwisePolicy::fromValue(
            Scope::CREATE_USERS
            + Scope::DELETE_USERS
            + Scope::IMPERSONATE_USERS
        );

        $this->assertTrue($policy->can(Scope::CREATE_USERS));
        $this->assertTrue($policy->can(Scope::DELETE_USERS));
        $this->assertTrue($policy->can(Scope::IMPERSONATE_USERS));
        $this->assertFalse($policy->can(Scope::MANAGE_PERMISSIONS));
    }

    public function testCannot(): void
    {
        $policy = BitwisePolicy::fromValue(
            Scope::CREATE_USERS
            + Scope::DELETE_USERS
            + Scope::IMPERSONATE_USERS
        );

        $this->assertFalse($policy->cannot(Scope::CREATE_USERS));
        $this->assertFalse($policy->cannot(Scope::DELETE_USERS));
        $this->assertFalse($policy->cannot(Scope::IMPERSONATE_USERS));
        $this->assertTrue($policy->cannot(Scope::MANAGE_PERMISSIONS));
    }

    public function testGrant(): void
    {
        $policy = BitwisePolicy::fromValue(
            Scope::CREATE_USERS
            + Scope::DELETE_USERS
            + Scope::IMPERSONATE_USERS
        );

        $policy->grant(Scope::MANAGE_PERMISSIONS);

        $this->assertTrue($policy->can(Scope::CREATE_USERS));
        $this->assertTrue($policy->can(Scope::DELETE_USERS));
        $this->assertTrue($policy->can(Scope::IMPERSONATE_USERS));
        $this->assertTrue($policy->can(Scope::MANAGE_PERMISSIONS));
    }

    public function testRevoke(): void
    {
        $policy = BitwisePolicy::fromValue(
            Scope::CREATE_USERS
            + Scope::DELETE_USERS
            + Scope::IMPERSONATE_USERS
        );

        $policy->revoke(Scope::IMPERSONATE_USERS);

        $this->assertTrue($policy->can(Scope::CREATE_USERS));
        $this->assertTrue($policy->can(Scope::DELETE_USERS));
        $this->assertFalse($policy->can(Scope::IMPERSONATE_USERS));
        $this->assertFalse($policy->can(Scope::MANAGE_PERMISSIONS));
    }

    public function testGrantIdempotency(): void
    {
        $policy = BitwisePolicy::fromValue(
            Scope::CREATE_USERS
            + Scope::DELETE_USERS
            + Scope::IMPERSONATE_USERS
        );

        $policy->grant(Scope::CREATE_USERS);

        $this->assertTrue($policy->can(Scope::CREATE_USERS));
        $this->assertTrue($policy->can(Scope::DELETE_USERS));
        $this->assertTrue($policy->can(Scope::IMPERSONATE_USERS));
        $this->assertFalse($policy->can(Scope::MANAGE_PERMISSIONS));
    }

    public function testRevokeIdempotency(): void
    {
        $policy = BitwisePolicy::fromValue(
            Scope::CREATE_USERS
            + Scope::DELETE_USERS
            + Scope::IMPERSONATE_USERS
        );

        $policy->revoke(Scope::MANAGE_PERMISSIONS);

        $this->assertTrue($policy->can(Scope::CREATE_USERS));
        $this->assertTrue($policy->can(Scope::DELETE_USERS));
        $this->assertTrue($policy->can(Scope::IMPERSONATE_USERS));
        $this->assertFalse($policy->can(Scope::MANAGE_PERMISSIONS));
    }
}

class Scope
{
    public const CREATE_USERS = 1;
    public const DELETE_USERS = 2;
    public const IMPERSONATE_USERS = 4;
    public const MANAGE_PERMISSIONS = 8;
}
