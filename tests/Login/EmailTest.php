<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Login;

use MNC\Account\Login\Email;
use MNC\Account\Login\InvalidEmailException;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    public function testEmailIsValid(): void
    {
        $email = Email::create('EMAIL@example.com');

        $this->assertSame('EMAIL@example.com', $email->getOriginal());
        $this->assertSame('email@example.com', $email->getCanonized());
    }

    public function testEmailIsInvalid(): void
    {
        $this->expectException(InvalidEmailException::class);
        Email::create('invalid-email');
    }

    public function testComparison(): void
    {
        $emailOne = Email::create('email@example.com');
        $emailTwo = Email::create('EMAIL@EXAMPLE.com');

        $this->assertTrue($emailOne->isEqualTo($emailTwo));
    }
}
