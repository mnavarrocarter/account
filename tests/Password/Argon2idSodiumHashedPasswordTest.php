<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Password;

use MNC\Account\Password\Argon2idSodiumPassword;
use MNC\Account\Password\InvalidPasswordException;
use PHPUnit\Framework\TestCase;

/**
 * Class Argon2idSodiumHashedPasswordTest.
 */
class Argon2idSodiumHashedPasswordTest extends TestCase
{
    public function testVerificationWithValidPasswordPasses(): void
    {
        $this->ensureSodiumExtensionIsLoaded();
        $this->expectNotToPerformAssertions();
        $password = Argon2idSodiumPassword::fromPlainPassword('password');
        $password->verify('password');
    }

    public function testVerificationWithInvalidPasswordFails(): void
    {
        $this->ensureSodiumExtensionIsLoaded();
        $password = Argon2idSodiumPassword::fromPlainPassword('password');
        $this->expectException(InvalidPasswordException::class);
        $password->verify('wrongPassword');
    }

    public function testVerificationFromHashWorks(): void
    {
        $this->ensureSodiumExtensionIsLoaded();
        $this->expectNotToPerformAssertions();
        $hash = Argon2idSodiumPassword::fromPlainPassword('password')->getValue();
        $password = Argon2idSodiumPassword::fromHash($hash);
        $password->verify('password');
    }

    private function ensureSodiumExtensionIsLoaded(): void
    {
        if (!extension_loaded('sodium')) {
            $this->markTestIncomplete();
        }
    }
}
