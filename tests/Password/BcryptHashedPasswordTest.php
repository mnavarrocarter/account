<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Password;

use MNC\Account\Password\BcryptPassword;
use MNC\Account\Password\InvalidPasswordException;
use PHPUnit\Framework\TestCase;

class BcryptHashedPasswordTest extends TestCase
{
    public function testVerificationWithValidPasswordPasses(): void
    {
        $this->expectNotToPerformAssertions();
        $password = BcryptPassword::fromPlainPassword('password');
        $password->verify('password');
    }

    public function testVerificationWithInvalidPasswordFails(): void
    {
        $password = BcryptPassword::fromPlainPassword('password');
        $this->expectException(InvalidPasswordException::class);
        $password->verify('wrongPassword');
    }

    public function testVerificationFromHashWorks(): void
    {
        $this->expectNotToPerformAssertions();
        $hash = BcryptPassword::fromPlainPassword('password')->getValue();
        $password = BcryptPassword::fromHash($hash);
        $password->verify('password');
    }
}
