<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Security;

use Cake\Chronos\Chronos;
use MNC\Account\Security\ExpiredTokenException;
use PHPUnit\Framework\TestCase;

class ExpiredTokenExceptionTest extends TestCase
{
    public function testGetter(): void
    {
        $now = Chronos::now();
        $exception = new ExpiredTokenException($now);
        $this->assertTrue($now->isSameDay($exception->getExpiredAt()));
    }
}
