<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Login;

/**
 * Class InvalidEmailException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class InvalidEmailException extends \DomainException
{
    /**
     * @var string
     */
    private $invalidEmail;

    /**
     * InvalidEmailException constructor.
     *
     * @param string $invalidEmail
     */
    public function __construct(string $invalidEmail)
    {
        parent::__construct('Invalid email');
        $this->invalidEmail = $invalidEmail;
    }

    /**
     * @return string
     */
    public function getInvalidEmail(): string
    {
        return $this->invalidEmail;
    }
}
