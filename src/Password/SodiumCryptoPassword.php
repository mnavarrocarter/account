<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Password;

/**
 * Class SodiumCryptoPassword.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
abstract class SodiumCryptoPassword implements Password
{
    /**
     * @var string
     */
    protected $hash;

    /**
     * SodiumCryptoPassword constructor.
     *
     * @param string $hash
     */
    protected function __construct(string $hash)
    {
        $this->hash = $hash;
    }

    /**
     * @param string $hash
     *
     * @return Password
     */
    public static function fromHash(string $hash): Password
    {
        return new static($hash);
    }

    public function verify(string $plainPassword): void
    {
        if (sodium_crypto_pwhash_str_verify($this->hash, $plainPassword)) {
            return;
        }
        throw new InvalidPasswordException($plainPassword);
    }

    public function getValue(): string
    {
        return $this->hash;
    }
}
