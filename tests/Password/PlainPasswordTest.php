<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Password;

use MNC\Account\Password\InvalidPasswordException;
use MNC\Account\Password\PlainPassword;
use PHPUnit\Framework\TestCase;

class PlainPasswordTest extends TestCase
{
    public function testVerificationWithValidPasswordPasses(): void
    {
        $password = PlainPassword::create('password');
        $password->verify('password');
        $this->expectNotToPerformAssertions();
    }

    public function testVerificationWithInvalidPasswordFails(): void
    {
        $password = PlainPassword::create('password');
        $this->expectException(InvalidPasswordException::class);
        $password->verify('wrongPassword');
    }

    public function testGetValue(): void
    {
        $password = PlainPassword::create('password');
        $this->assertSame('password', $password->getValue());
    }
}
