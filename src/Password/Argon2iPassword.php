<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Password;

/**
 * Class Argon2iPassword.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class Argon2iPassword extends HashedPassword
{
    /**
     * @param string $plainPassword
     * @param int    $cost
     *
     * @return Password
     */
    public static function fromPlainPassword(string $plainPassword, int $cost = 12): Password
    {
        return new static(password_hash($plainPassword, PASSWORD_ARGON2I, [
            'cost' => $cost,
        ]));
    }
}
