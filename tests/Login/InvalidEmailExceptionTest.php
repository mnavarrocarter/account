<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Login;

use MNC\Account\Login\InvalidEmailException;
use PHPUnit\Framework\TestCase;

class InvalidEmailExceptionTest extends TestCase
{
    public function testGetter(): void
    {
        $exception = new InvalidEmailException('invalid-email');
        $this->assertSame('invalid-email', $exception->getInvalidEmail());
    }
}
