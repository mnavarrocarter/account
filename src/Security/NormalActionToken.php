<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Security;

/**
 * This class represents an Action Token.
 *
 * Action Token is an element of many accounts in the web that allow for a 2-step
 * sensitive action verification by means of sending a token.
 *
 * A Token (random string) is generated and returned to the user. Usually sent
 * via email in the form of a uri. When the user uses that link, we know that's him for
 * sure and we can proceed with the sensitive action.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class NormalActionToken implements ActionToken
{
    /**
     * @var string
     */
    protected $token;

    /**
     * NormalActionToken constructor.
     */
    protected function __construct()
    {
        $this->token = bin2hex(random_bytes(16));
    }

    /**
     * @param int $ttl
     *
     * @return NormalActionToken
     */
    public static function generate(): ActionToken
    {
        return new self();
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @throws InvalidTokenException when token is not the same
     */
    public function verify(string $token): void
    {
        if ($token !== $this->token) {
            throw new InvalidTokenException($token);
        }
    }
}
