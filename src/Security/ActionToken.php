<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Security;

/**
 * Interface ActionToken.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface ActionToken
{
    /**
     * Casts the token into a string.
     *
     * @return string
     */
    public function toString(): string;

    /**
     * Verifies the token.
     *
     * @param string $token
     */
    public function verify(string $token): void;
}
