<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Security;

use MNC\Account\Security\InvalidTokenException;
use MNC\Account\Security\NormalActionToken;
use PHPUnit\Framework\TestCase;

class ActionTokenTest extends TestCase
{
    public function testGeneration(): void
    {
        $token = NormalActionToken::generate();
        $token2 = NormalActionToken::generate();

        $this->assertNotSame($token->toString(), $token2->toString());
    }

    public function testRightTokenIsVerified(): void
    {
        $this->expectNotToPerformAssertions();
        $token = NormalActionToken::generate();
        $token->verify($token->toString());
    }

    public function testDifferentTokenIsInvalid(): void
    {
        $token = NormalActionToken::generate();

        $this->expectException(InvalidTokenException::class);
        $token->verify('wrong-token');
    }
}
