<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Security;

use Cake\Chronos\Chronos;

/**
 * Class ExpirableActionToken.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ExpirableActionToken extends NormalActionToken
{
    /**
     * @var Chronos
     */
    protected $expiresAt;

    /**
     * ExpirableActionToken constructor.
     *
     * @param int $ttl
     */
    protected function __construct(int $ttl)
    {
        parent::__construct();
        $this->expiresAt = Chronos::createFromTimestamp(time() + $ttl);
    }

    /**
     * @param int $ttl
     *
     * @return ActionToken
     */
    public static function generate(int $ttl = 3600): ActionToken
    {
        return new self($ttl);
    }

    /**
     * @return Chronos
     */
    public function getExpiresAt(): Chronos
    {
        return $this->expiresAt;
    }

    /**
     * @param string $token
     */
    public function verify(string $token): void
    {
        if ($this->expiresAt->isPast()) {
            throw new ExpiredTokenException($this->expiresAt);
        }
        parent::verify($token);
    }
}
