<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Login;

/**
 * Class Username.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class Username extends CanonizedString
{
}
