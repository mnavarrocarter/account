<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Security;

use MNC\Account\Security\ExpirableActionToken;
use MNC\Account\Security\ExpiredTokenException;
use PHPUnit\Framework\TestCase;

class ExpirableActionTokenTest extends TestCase
{
    public function testThatTokenExpires(): void
    {
        $token = ExpirableActionToken::generate(-1);
        $this->expectException(ExpiredTokenException::class);
        $token->verify($token->toString());
    }

    public function testThatTokenValidatesCorrectly(): void
    {
        $this->expectNotToPerformAssertions();
        $token = ExpirableActionToken::generate();
        $token->verify($token->toString());
    }
}
