<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Password;

use MNC\Account\Password\Argon2iPassword;
use MNC\Account\Password\InvalidPasswordException;
use PHPUnit\Framework\TestCase;

class Argon2iHashedPasswordTest extends TestCase
{
    public function testVerificationWithValidPasswordPasses(): void
    {
        $this->ensureArgonConstantIsDefined();
        $this->expectNotToPerformAssertions();
        $password = Argon2iPassword::fromPlainPassword('password');
        $password->verify('password');
    }

    public function testVerificationWithInvalidPasswordFails(): void
    {
        $this->ensureArgonConstantIsDefined();
        $password = Argon2iPassword::fromPlainPassword('password');
        $this->expectException(InvalidPasswordException::class);
        $password->verify('wrongPassword');
    }

    public function testVerificationFromHashWorks(): void
    {
        $this->ensureArgonConstantIsDefined();
        $this->expectNotToPerformAssertions();
        $hash = Argon2iPassword::fromPlainPassword('password')->getValue();
        $password = Argon2iPassword::fromHash($hash);
        $password->verify('password');
    }

    private function ensureArgonConstantIsDefined(): void
    {
        if (!\defined('PASSWORD_ARGON2I')) {
            $this->markTestIncomplete();
        }
    }
}
