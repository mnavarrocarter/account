<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Login;

use MNC\Account\Login\Username;
use PHPUnit\Framework\TestCase;

class UsernameTest extends TestCase
{
    public function testUsernameIsValid(): void
    {
        $username = Username::create('USERNAME');

        $this->assertSame('USERNAME', $username->getOriginal());
        $this->assertSame('username', $username->getCanonized());
    }

    public function testComparison(): void
    {
        $usernameOne = Username::create('USERNAME');
        $usernameTwo = Username::create('username');

        $this->assertTrue($usernameOne->isEqualTo($usernameTwo));
    }
}
