<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Security;

use Cake\Chronos\Chronos;

/**
 * Class ExpiredTokenException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ExpiredTokenException extends \DomainException
{
    /**
     * @var Chronos
     */
    private $expiredAt;

    /**
     * ExpiredTokenException constructor.
     *
     * @param Chronos $expiredAt
     */
    public function __construct(Chronos $expiredAt)
    {
        parent::__construct('The provided token has already expired');
        $this->expiredAt = $expiredAt;
    }

    /**
     * @return Chronos
     */
    public function getExpiredAt(): Chronos
    {
        return $this->expiredAt;
    }
}
