Account
=======

Account abstractions for your PHP projects.

## Installation

```bash
$ composer require mnavarro/account-utils
```

## What is this?

It is a series of classes that help building Account logic, like login, passwords
and some other stuff.