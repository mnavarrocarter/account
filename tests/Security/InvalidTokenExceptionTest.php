<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Security;

use MNC\Account\Security\InvalidTokenException;
use PHPUnit\Framework\TestCase;

class InvalidTokenExceptionTest extends TestCase
{
    public function testCreationAndGetter(): void
    {
        $exception = new InvalidTokenException('wrongToken');

        $this->assertSame('wrongToken', $exception->getWrongToken());
    }
}
