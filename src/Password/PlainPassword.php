<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Password;

/**
 * Class PlainPassword.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class PlainPassword implements Password
{
    /**
     * @var string
     */
    private $value;

    /**
     * PlainPassword constructor.
     *
     * @param string $plainPassword
     */
    protected function __construct(string $plainPassword)
    {
        $this->value = $plainPassword;
    }

    /**
     * @param string $plainPassword
     *
     * @return Password
     */
    public static function create(string $plainPassword): Password
    {
        return new self($plainPassword);
    }

    /**
     * @param string $plainPassword
     */
    public function verify(string $plainPassword): void
    {
        if ($plainPassword === $this->value) {
            return;
        }
        throw new InvalidPasswordException($plainPassword);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
