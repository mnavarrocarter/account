<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Password;

/**
 * Interface Password.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface Password
{
    /**
     * @param string $plainPassword
     *
     * @throws InvalidPasswordException on invalid password
     */
    public function verify(string $plainPassword): void;

    /**
     * @return string
     */
    public function getValue(): string;
}
