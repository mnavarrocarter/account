<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Util;

/**
 * Class MbStringCanonicalizer.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class MbStringCanonicalizer implements Canonicalizer
{
    /**
     * @param string $string
     *
     * @return string
     */
    public function canonicalize(string $string): string
    {
        $encoding = mb_detect_encoding($string);
        $result = $encoding
            ? mb_convert_case($string, MB_CASE_LOWER, $encoding)
            : mb_convert_case($string, MB_CASE_LOWER);

        return $result;
    }
}
