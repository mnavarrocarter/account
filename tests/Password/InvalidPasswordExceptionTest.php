<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Password;

use MNC\Account\Password\InvalidPasswordException;
use PHPUnit\Framework\TestCase;

class InvalidPasswordExceptionTest extends TestCase
{
    public function testCreationAndGetters(): void
    {
        $exception = new InvalidPasswordException('wrongPassword');
        $this->assertSame('wrongPassword', $exception->getWrongPassword());
    }
}
