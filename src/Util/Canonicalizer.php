<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Util;

/**
 * Interface Canonicalizer.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface Canonicalizer
{
    /**
     * @param string $string
     *
     * @return string
     */
    public function canonicalize(string $string): string;
}
