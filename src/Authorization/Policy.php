<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Authorization;

/**
 * A Policy represents a group of scopes.
 *
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface Policy
{
    /**
     * @param mixed $scope
     *
     * @return bool
     */
    public function can($scope): bool;

    /**
     * @param mixed $scope
     *
     * @return bool
     */
    public function cannot($scope): bool;

    /**
     * @param mixed $scope
     *
     * @return Policy
     */
    public function grant($scope): Policy;

    /**
     * @param mixed $scope
     *
     * @return Policy
     */
    public function revoke($scope): Policy;

    /**
     * @return mixed
     */
    public function getValue();
}
