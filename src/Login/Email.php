<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Login;

use Assert\Assertion;
use Assert\AssertionFailedException;
use MNC\Account\Util\Canonicalizer;

/**
 * Class Email.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class Email extends CanonizedString
{
    /**
     * Email constructor.
     *
     * @param string        $string
     * @param Canonicalizer $canonicalizer
     */
    protected function __construct(string $string, Canonicalizer $canonicalizer)
    {
        try {
            Assertion::email($string);
        } catch (AssertionFailedException $exception) {
            throw new InvalidEmailException($string);
        }
        parent::__construct($string, $canonicalizer);
    }
}
