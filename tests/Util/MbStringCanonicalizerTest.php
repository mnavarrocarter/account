<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Tests\Util;

use MNC\Account\Util\MbStringCanonicalizer;
use PHPUnit\Framework\TestCase;

class MbStringCanonicalizerTest extends TestCase
{
    public function testCanonization(): void
    {
        $canonicalizer = new MbStringCanonicalizer();

        $this->assertSame('some-email@gmail.com', $canonicalizer->canonicalize('SOME-EMAIL@GMAIL.COM'));
    }

    public function testPlusSymbols(): void
    {
        $canonicalizer = new MbStringCanonicalizer();

        $this->assertSame('some-email+1@gmail.com', $canonicalizer->canonicalize('SOME-EMAIL+1@GMAIL.COM'));
    }
}
