<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Authorization;

/**
 * Bitwise policy implements policies using bit comparison, which is really
 * efficient in terms of I/O and storage.
 *
 * Scopes definition values MUST be a sequence of integers duplicated, starting at one.
 * You can, for example, have a class called Scope with public constants that will
 * define your application scopes like this:
 *
 * Scope::CREATE_USERS = 1
 * Scope::DELETE_USERS = 2
 * Scope::EDIT_USERS = 4
 * Scope::MANAGE_PERMISSIONS = 8
 * Scope::IMPERSONATE_USERS = 16
 *
 * The maximum number of Scopes you can store with this system in a 64 bit architecture
 * is pretty high, around the order of 600 different scopes.
 *
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class BitwisePolicy implements Policy
{
    /**
     * @var int
     */
    private $value;

    protected function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @param $value
     *
     * @return Policy
     */
    public static function fromValue(int $value): Policy
    {
        return new static($value);
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $scope
     *
     * @return bool
     */
    public function can($scope): bool
    {
        $this->ensureValueIsAllowed($scope);

        return $this->value & $scope;
    }

    /**
     * @param int $scope
     *
     * @return bool
     */
    public function cannot($scope): bool
    {
        return !$this->can($scope);
    }

    /**
     * @param int $scope
     *
     * @return Policy
     */
    public function grant($scope): Policy
    {
        if ($this->cannot($scope)) {
            $this->value += $scope;
        }

        return $this;
    }

    /**
     * @param int $scope
     *
     * @return Policy
     */
    public function revoke($scope): Policy
    {
        if ($this->can($scope)) {
            $this->value -= $scope;
        }

        return $this;
    }

    /**
     * @param int $scope
     */
    private function ensureValueIsAllowed(int $scope): void
    {
        // The argument type hinting does the job.
    }
}
