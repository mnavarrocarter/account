<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Security;

/**
 * Class InvalidTokenException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class InvalidTokenException extends \DomainException
{
    /**
     * @var string
     */
    private $wrongToken;

    /**
     * InvalidTokenException constructor.
     *
     * @param string $wrongToken
     */
    public function __construct(string $wrongToken)
    {
        parent::__construct('The provided token does not match');
        $this->wrongToken = $wrongToken;
    }

    /**
     * @return string
     */
    public function getWrongToken(): string
    {
        return $this->wrongToken;
    }
}
