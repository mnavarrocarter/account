<?php

/*
 * This file is part of the MNC\Account library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\Account\Login;

use MNC\Account\Util\Canonicalizer;
use MNC\Account\Util\MbStringCanonicalizer;

/**
 * Class CanonizedString.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
abstract class CanonizedString
{
    /**
     * @var string
     */
    protected $original;
    /**
     * @var string
     */
    protected $canonized;

    /**
     * Username constructor.
     *
     * @param string        $value
     * @param Canonicalizer $canonicalizer
     */
    protected function __construct(string $value, Canonicalizer $canonicalizer)
    {
        $this->original = $value;
        $this->canonized = $canonicalizer->canonicalize($value);
    }

    /**
     * @param string             $value
     * @param Canonicalizer|null $canonicalizer
     *
     * @return CanonizedString
     */
    public static function create(string $value, Canonicalizer $canonicalizer = null): CanonizedString
    {
        return new static($value, $canonicalizer ?? new MbStringCanonicalizer());
    }

    /**
     * @return string
     */
    public function getOriginal(): string
    {
        return $this->original;
    }

    /**
     * @return string
     */
    public function getCanonized(): string
    {
        return $this->canonized;
    }

    /**
     * @param CanonizedString $string
     *
     * @return bool
     */
    public function isEqualTo(CanonizedString $string): bool
    {
        return $string->canonized === $this->canonized;
    }
}
